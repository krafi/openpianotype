import pygame
import numpy as np
import time

# Initialize Pygame
pygame.init()

# Screen dimensions
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
KEY_WIDTH = 100
KEY_HEIGHT = 50

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Note keys and their positions
keys = ['G', 'H', 'J', 'K', 'L', ';']
piano_notes = ['C', 'D', 'E', 'F', 'G', 'A']

# Map keys to piano notes
key_to_note = {
    'G': 'C',
    'H': 'D',
    'J': 'E',
    'K': 'F',
    'L': 'G',
    ';': 'A'
}

key_positions = {key: i * KEY_WIDTH for i, key in enumerate(keys)}

# Frequencies for each key (C major scale and the octave above C)
frequencies = {
    'C': 261.63,
    'D': 293.66,
    'E': 329.63,
    'F': 349.23,
    'G': 392.00,
    'A': 440.00
}

# Set up the display
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Piano Game")

# Font for displaying keys
font = pygame.font.Font(None, 74)

# Function to generate a piano key sound
def generate_piano_key_sound(frequency, duration=1.0, sample_rate=44100):
    t = np.linspace(0, duration, int(sample_rate * duration), endpoint=False)
    waveform = 0.5 * np.sin(2 * np.pi * frequency * t)
    sound_array = np.int16(waveform * 32767).reshape(-1, 1)
    sound_array = np.tile(sound_array, (1, 2))  # Duplicate the array for stereo sound
    sound = pygame.sndarray.make_sound(sound_array)
    return sound

# Create sound objects for each key
key_sounds = {note: generate_piano_key_sound(frequencies[note]) for note in piano_notes}

# Music sheet for "Twinkle, Twinkle, Little Star" (note, duration in seconds)
music_sheet = [
    ('C', 0.5), ('C', 0.5), ('G', 0.5), ('G', 0.5), ('A', 0.5), ('A', 0.5), ('G', 1.0),
    ('F', 0.5), ('F', 0.5), ('E', 0.5), ('E', 0.5), ('D', 0.5), ('D', 0.5), ('C', 1.0),
    ('G', 0.5), ('G', 0.5), ('F', 0.5), ('F', 0.5), ('E', 0.5), ('E', 0.5), ('D', 1.0),
    ('G', 0.5), ('G', 0.5), ('F', 0.5), ('F', 0.5), ('E', 0.5), ('E', 0.5), ('D', 1.0),
    ('C', 0.5), ('C', 0.5), ('G', 0.5), ('G', 0.5), ('A', 0.5), ('A', 0.5), ('G', 1.0),
    ('F', 0.5), ('F', 0.5), ('E', 0.5), ('E', 0.5), ('D', 0.5), ('D', 0.5), ('C', 1.0)
]

# Function to create notes based on the melody of "Twinkle, Twinkle, Little Star"
def create_notes():
    notes = []
    current_time = 0
    for note, duration in music_sheet:
        notes.append((note, current_time))
        current_time += duration
    return notes

# Define a Note class
class Note:
    def __init__(self, note, start_time):
        self.note = note
        self.key = [key for key, n in key_to_note.items() if n == note][0]  # Find the corresponding key
        self.x = key_positions[self.key]
        self.y = -KEY_HEIGHT
        self.start_time = start_time

    def draw(self):
        pygame.draw.rect(screen, RED, (self.x, self.y, KEY_WIDTH, KEY_HEIGHT))
        text = font.render(self.key, True, WHITE)
        screen.blit(text, (self.x + (KEY_WIDTH // 2 - text.get_width() // 2), self.y + (KEY_HEIGHT // 2 - text.get_height() // 2)))

    def update(self, current_time):
        # Move the note down based on elapsed time
        elapsed_time = current_time - self.start_time
        self.y = int(elapsed_time * 100)  # Speed of falling notes

# Game variables
notes = [Note(note, start_time) for note, start_time in create_notes()]
score = 0
clock = pygame.time.Clock()
running = True
start_time = time.time()

# Main game loop
while running:
    current_time = time.time() - start_time

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            key = pygame.key.name(event.key).upper()
            if key in key_to_note:
                note = key_to_note[key]
                for note_obj in notes:
                    if note_obj.note == note and SCREEN_HEIGHT - KEY_HEIGHT <= note_obj.y <= SCREEN_HEIGHT:
                        key_sounds[note].play()
                        notes.remove(note_obj)
                        score += 1
                        break

    # Update and draw notes
    screen.fill(BLACK)
    for note in notes:
        note.update(current_time)
        note.draw()
        if note.y > SCREEN_HEIGHT:
            notes.remove(note)

    # Display score
    score_text = font.render(f"Score: {score}", True, WHITE)
    screen.blit(score_text, (10, 10))

    pygame.display.flip()
    clock.tick(60)

pygame.quit()
